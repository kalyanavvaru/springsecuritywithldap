App.Views.ContractorsCollection = Backbone.View.extend({
	el: 'li',
	initialize: function(){
		_.bindAll(this, 'initialize');
		this.collection = new App.Collections.ContractorsCollection;
		this.collection.on('reset', this.render, this);
		this.collection.fetch();
		/*this.collection.models.push(new App.Models.Contractor({id:"2"}));
		console.log(this.collection.models[0]);*/
	},
	render: function(){
		_.bindAll(this, 'render');
		var contractors = this.collection.models[0].attributes; 
		var template = _.template( $("#posts_template").html(), contractors );
		this.$el.html( template );
		return this;
	}
	
});