package au.com.contractorbay.exceptions;

public class WrapperException extends Exception {

	private static final long serialVersionUID = 5450095355263159062L;
	public WrapperException(final String pMessage) {
		super(pMessage);
	}
}
