package au.com.contractorbay.security;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import au.com.contractorbay.BaseTestConfig;
import au.com.contractorbay.util.MockPerformer;
import au.com.contractorbay.util.MockResultMatcher;
import au.com.contractorbay.util.MockUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = BaseTestConfig.class)
public class SecurityFrontControllerTest {

	@Autowired
	private WebApplicationContext appContext;

	private MockMvc mockMvc;

	public WebApplicationContext getAppContext() {
		return this.appContext;
	}

	public void setAppContext(final WebApplicationContext pAppContext) {
		this.appContext = pAppContext;
	}

	public MockMvc getMockMvc() {
		return this.mockMvc;
	}

	public void setMockMvc(final MockMvc pMockMvc) {
		this.mockMvc = pMockMvc;
	}

	@Before
	public void init() {
		this.mockMvc = new MockUtil(this.appContext).build();
	}

	@Test
	public void testHome() throws Exception {
		final MockPerformer mkPerformer = new MockPerformer(this.mockMvc, "/secured");
		Assert.assertNotNull(mkPerformer.getResultActions());
		mkPerformer.expect(MockResultMatcher.isOK());
		mkPerformer.expect(MockResultMatcher.name("secured_page"));
	}
	@Test
	public void testAuthorizeUser() throws Exception {
		final MockPerformer mkPerformer = new MockPerformer(this.mockMvc, "/secured/users");
		Assert.assertNotNull(mkPerformer.getResultActions());
		mkPerformer.expect(MockResultMatcher.isOK());
		mkPerformer.expect(MockResultMatcher.name("user_home"));
	}

	@Test
	public void testAuthorizeAdmin() throws Exception {
		final MockPerformer mkPerformer = new MockPerformer(this.mockMvc, "/secured/admins");
		Assert.assertNotNull(mkPerformer.getResultActions());
		mkPerformer.expect(MockResultMatcher.isOK());
		mkPerformer.expect(MockResultMatcher.name("admin_home"));
	}

}
