package au.com.contractorbay.util;

import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.StatusResultMatchers;
import org.springframework.test.web.servlet.result.ViewResultMatchers;

public final class MockResultMatcher {
	private static final StatusResultMatchers statusMatchers;
	private static final ViewResultMatchers viewMatchers;

	private MockResultMatcher() {
	}

	static {
		statusMatchers = MockMvcResultMatchers.status();
		viewMatchers = MockMvcResultMatchers.view();
	}

	public static ResultMatcher isOK() {
		return statusMatchers.isOk();
	}

	public static ResultMatcher name(final String pName) {
		return viewMatchers.name(pName);
	}
}
