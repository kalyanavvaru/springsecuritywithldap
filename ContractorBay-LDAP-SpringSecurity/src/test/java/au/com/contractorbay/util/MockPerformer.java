package au.com.contractorbay.util;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
public final class MockPerformer {
	private ResultActions resultActions;

	public MockPerformer(final MockMvc pMockMvc, final String pUrl) throws Exception { // NOPMD
		final MockHttpServletRequestBuilder mockRqBldr = MockMvcRequestBuilders.get(pUrl);
		this.resultActions = pMockMvc.perform(mockRqBldr);
	}

	public void expect(final ResultMatcher pResultMatcher) throws Exception { // NOPMD
		this.resultActions.andExpect(pResultMatcher);
	}

	public ResultActions getResultActions() {
		return this.resultActions;
	}

	public void setResultActions(final ResultActions pResultActions) {
		this.resultActions = pResultActions;
	}

}
