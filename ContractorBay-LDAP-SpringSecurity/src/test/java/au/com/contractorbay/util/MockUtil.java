package au.com.contractorbay.util;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public final class MockUtil {
	private DefaultMockMvcBuilder<DefaultMockMvcBuilder<?>> mockBuilder;

	public MockUtil(final WebApplicationContext pAppContext) {
		this.mockBuilder = MockMvcBuilders.webAppContextSetup(pAppContext);
	}

	public MockMvc build() {
		return this.mockBuilder.build();
	}

	public DefaultMockMvcBuilder<DefaultMockMvcBuilder<?>> getMockBuilder() {
		return this.mockBuilder;
	}

	public void setMockBuilder(final DefaultMockMvcBuilder<DefaultMockMvcBuilder<?>> pMockBuilder) {
		this.mockBuilder = pMockBuilder;
	}

}
