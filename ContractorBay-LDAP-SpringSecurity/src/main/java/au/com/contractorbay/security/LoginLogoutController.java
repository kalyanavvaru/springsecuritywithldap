package au.com.contractorbay.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/auth")
public class LoginLogoutController {

	private static final Logger logger = LoggerFactory.getLogger(SecurityFrontController.class);

	/**
	 * Handles and retrieves the denied JSP page. This is shown whenever a regular user tries to access an admin only page.
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/denied", method = RequestMethod.GET)
	public final String getDeniedPage() {

		logger.debug("Received request to show denied page");

		return "deniedpage";
	}

	/**
	 * Handles and retrieves the login JSP page.
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public final String getLoginPage(@RequestParam(value = "error", required = false) final boolean pError, final ModelMap pModel) {

		logger.debug("Received request to show login page");

		if (pError) {
			pModel.put("error", "You have entered an invalid username or password!");
		} else {
			pModel.put("error", "");
		}

		return "loginpage";
	}
}