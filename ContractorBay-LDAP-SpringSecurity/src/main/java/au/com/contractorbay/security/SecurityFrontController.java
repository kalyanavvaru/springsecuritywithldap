package au.com.contractorbay.security;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.contractorbay.util.ModelUtil;

@Controller
@RequestMapping(value = "/secured")
public class SecurityFrontController {

	private static final Logger logger = LoggerFactory.getLogger(SecurityFrontController.class);

	@RequestMapping(value = "/admins", method = RequestMethod.GET)
	public final String authorizeAdmin(final Locale pLocale, final Model pModel) {

		logger.info("Welcome home! The client locale is {}.", pLocale);

		ModelUtil.populateDate(pModel, pLocale);

		return "admin_home";
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public final String authorizeUser(final Locale pLocale, final Model pModel) {

		logger.info("Welcome home! The client locale is {}.", pLocale);
		ModelUtil.populateDate(pModel, pLocale);

		return "user_home";
	}

	@RequestMapping(method = RequestMethod.GET)
	public final String home(final Locale pLocale, final Model pModel) {

		logger.info("Welcome home! The client locale is {}.", pLocale);

		ModelUtil.populateDate(pModel, pLocale);

		return "secured_page";
	}

}
