package au.com.contractorbay.util;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.ui.Model;

/**
 * This class has util methods that can be re-usable like populating date in view model.
 * 
 * @author kalyan
 * 
 */
public final class ModelUtil {

	private ModelUtil() {
		throw new IllegalAccessError();
	}

	public static void populateDate(final Model pModel, final Locale pLocale) {
		final Date date = new Date();

		final DateTimeUtil dateTimeUtils = new DateTimeUtil(pLocale, DateFormat.LONG);

		pModel.addAttribute("serverTime", dateTimeUtils.getString(date));
	}

}
