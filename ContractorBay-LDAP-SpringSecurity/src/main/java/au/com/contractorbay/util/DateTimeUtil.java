package au.com.contractorbay.util;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
/**
 * This class has static helper methods for date related operations.
 * 
 * @author kalyan
 * 
 */
public final class DateTimeUtil {

	private DateFormat dateFormat;

	public DateTimeUtil(final Locale pLocale, final int pFormat) {
		this.dateFormat = DateFormat.getDateInstance(pFormat, pLocale);
	}

	public DateFormat getDateFormat() {
		return this.dateFormat;

	}

	public String getString(final Date pDate) {
		return this.dateFormat.format(pDate);
	}

	public void setDateFormat(final DateFormat pDateFormat) {
		this.dateFormat = pDateFormat;
	}

}
