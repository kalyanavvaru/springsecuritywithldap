package au.com.contractorbay.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.contractorbay.util.DateTimeUtil;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public final String home(final Locale pLocale, final Model pModel) {
		logger.info("Welcome home! The client locale is {}.", pLocale);

		final Date date = new Date();

		final DateTimeUtil dateTimeUtils = new DateTimeUtil(pLocale, DateFormat.LONG);

		pModel.addAttribute("serverTime", dateTimeUtils.getString(date));

		return "home";
	}

}
