<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
<sec:authorize access="isAuthenticated()">
	<a href="secured">View secured links</a> <p/>
</sec:authorize>
	
<sec:authorize access="!isAuthenticated()">
	<a href="auth/login">Login</a>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
	<a href="auth/logout">Logout</a>
</sec:authorize>
</body>
</html>
