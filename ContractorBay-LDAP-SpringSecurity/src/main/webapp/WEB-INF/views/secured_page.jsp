<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
	<head></head>
	<body>
		Users click <a href="secured/users">here</a><p/>
		<sec:authorize access="hasRole('ROLE_ADMIN')">	
			Admins click <a href="secured/admins">here</a>
		</sec:authorize>
	</body>
</html>