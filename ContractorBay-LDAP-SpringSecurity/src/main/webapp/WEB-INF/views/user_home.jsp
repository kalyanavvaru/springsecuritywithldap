<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
	<head></head>
	<body>
		<h1>Welcome User</h1>
		<sec:authorize access="isAuthenticated()">
			<a href="<c:url value="/auth/logout"/>">Logout</a>
		</sec:authorize>
	</body>
</html>