package au.com.contractorbay;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.contractorbay.domain.Contractor;
import au.com.contractorbay.service.ContractorService;

@Controller
@RequestMapping("/api")
public class ContractorController {

	ContractorService contractorService;

	@Autowired
	public ContractorController(final ContractorService contractorService) {
		this.contractorService = contractorService;
	}

	@RequestMapping("contractors/{id}")
	@ResponseBody
	public Contractor getById(@PathVariable final long id) {
		return this.contractorService.getById(id);
	}
	/*
	 * same as above method, but is mapped to /contractor?id= rather than
	 * /contractor/{id}
	 */
	@RequestMapping(value = "contractors", params = "id")
	@ResponseBody
	public Contractor getByIdFromParam(@RequestParam final long id) {
		return this.contractorService.getById(id);
	}

	@RequestMapping(value = "person", method = RequestMethod.POST)
	@ResponseBody
	public String savePerson(final Contractor contractor) {
		this.contractorService.save(contractor);
		return "Saved person: " + contractor.toString();
	}

	@RequestMapping("contractors/")
	@ResponseBody
	public Set<Contractor> getAll() {
		return this.contractorService.getAll();
	}

}
