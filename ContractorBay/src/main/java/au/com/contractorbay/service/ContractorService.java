package au.com.contractorbay.service;

import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import au.com.contractorbay.domain.Contractor;

@Service
public class ContractorService {

	public Contractor getById(final long id) {
		final Contractor contractor = new Contractor();
		contractor.setId(id);
		contractor.setName("Kalyan");
		contractor.setSurname("Avvaru");
		return contractor;
	}

	public void save(final Contractor contractor) {
		System.out.println("contractor is saved");
	}

	public Set<Contractor> getAll() {
		final Set<Contractor> allContractors = new LinkedHashSet<Contractor>();
		final Contractor contractor = new Contractor();
		contractor.setId(1);
		contractor.setName("Kalyan");
		contractor.setSurname("Avvaru");
		allContractors.add(contractor);

		final Contractor contractor2 = new Contractor();
		contractor2.setId(2);
		contractor2.setName("Min");
		contractor2.setSurname("Huang");
		allContractors.add(contractor2);
		return allContractors;
	}

}
