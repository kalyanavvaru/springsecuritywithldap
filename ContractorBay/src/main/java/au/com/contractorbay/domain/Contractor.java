package au.com.contractorbay.domain;

public class Contractor {
	public long id;
	public String name;
	public String surname;

	public long getId() {
		return this.id;
	}
	public void setId(final long id) {
		this.id = id;
	}
	public String getName() {
		return this.name;
	}
	public void setName(final String name) {
		this.name = name;
	}
	public String getSurname() {
		return this.surname;
	}
	public void setSurname(final String surname) {
		this.surname = surname;
	}
}
