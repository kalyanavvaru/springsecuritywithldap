App.Collections.ContractorsCollection = Backbone.Collection.extend({
	model : App.Models.Contractor,
	url : 'api/contractors/',
	initialize : function() {
		console.log("==> NEW ContractorsCollection INITIALISED");
	}
});
//var col = new App.Collections.ContractorsCollection();