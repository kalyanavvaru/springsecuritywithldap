App.Views.Contractor = Backbone.View.extend({
	el: 'li',
	initialize: function(){
		_.bindAll(this, 'initialize');
		
		this.model = new App.Models.Contractor({id:"2"});
		
		this.model.on('reset', this.render, this);
		this.model.fetch();
		console.log('initialize contractor');
		console.log(this.model);
		
		/*var col = new App.Collections.Blog();
		var model = new App.Models.Post({id : 2}, {collection : col});
		model.fetch();*/
	},
	render: function(){
		_.bindAll(this, 'render');
		//console.log(this.collection);
		var contractor = this.models.attributes; //actual response as the format is not the best
		console.log(contractor);
		var template = _.template( $("#posts_template").html(), contractor );
		this.$el.html( template );
		return this;
	}
	
});