App.Models.Contractor = Backbone.Model.extend({
	defaults : {
		"id" : null,
		"name" : "",
	},
	urlRoot : 'api/contractors/',
	initialize : function() {
		console.log("==> NEW Contractor INITIALISED");
	}
});