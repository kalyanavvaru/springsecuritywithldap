<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <!-- general jquery libraries -->
    
 	<script src="resources/lib/jquery.min.js"></script>
    <script src="resources/lib/json2.js"></script>
    <script src="resources/lib/underscore-min.js"></script>
    <script src="resources/lib/backbone-min.js"></script>

	<!-- backbone MV* -->
    <script src="resources/controllers/contractormvw.js"></script>
    <script src="resources/models/contractor.js"></script>
    <script src="resources/collections/contractorsCollection.js"></script>
	<script src="resources/views/contractorView.js"></script>
    <script src="resources/views/contractorCollectionView.js"></script>
    
    <script type="text/javascript">
    	<!--bootstrap the app-->
    	//App.contractorCollectionView = new App.Views.ContractorsCollection({el: $('#posts-container')});
    	App.contractorView = new App.Views.Contractor({el: $('#posts-container')});
    	
    </script>
</head>
<body>
<div id="posts-container"></div>
</body> 
